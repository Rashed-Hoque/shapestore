﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RashedHoque.ShapeStore.Objects
{
    [Serializable]
    public class ShoppingCart
    {
        public string CustomerId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CcType { get; set; }
        public string CcNumber { get; set; }
        public string CcExpiration { get; set; }
        public List<ShoppingCartItem> Items { get; set; }

        public ShoppingCart()
        {
            DateCreated = DateTime.Now;
            Items = new List<ShoppingCartItem>();
        }

    }
}
