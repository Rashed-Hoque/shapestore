﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RashedHoque.ShapeStore.Objects
{
    [Serializable]
    public class ShoppingCartItem
    {
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public float PriceWhenAdded { get; set; }
        public DateTime DateAdded { get; set; }
    }
}
